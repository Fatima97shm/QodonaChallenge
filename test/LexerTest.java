import org.junit.Before;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class LexerTest
{
    private Lexer lexer;
    private String directoryPath;
    private String fileName;
    private String filePath;
    @Test
    @DisplayName("Tokenize Input File")
    void processLexer() throws IOException
    {
        directoryPath = "/Users/fatemehshamsoddiniardekani/Desktop/";
        fileName = "test.java";
        filePath = directoryPath + fileName;
        lexer = new Lexer(filePath);
        ArrayList<Token> tokenStream =lexer.process();
        for(Token token: tokenStream)
        {
            System.out.println("Token Type: "+ token.getType()+ "  Token Value: "+token.getValue());
        }
    }
}