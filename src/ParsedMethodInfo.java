public class ParsedMethodInfo {
    private String methodName;
    private int complexityScore=0;
    private boolean isCamelCase;

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public void setCamelCase(boolean camelCase)
    {
        isCamelCase = camelCase;
    }

    public void setComplexityScore(int complexityScore)
    {
        this.complexityScore = complexityScore;
    }

    public int getComplexityScore() {
        return complexityScore;
    }

    public String getMethodName()
    {
        return methodName;
    }
    public boolean isCamelCase()
    {
        return isCamelCase;
    }
}
