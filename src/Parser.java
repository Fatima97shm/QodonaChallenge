import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Parser implements Filter
{
    // the input we get from lexer
    private ArrayList<Token> tokenStream ;
    private int currentTokenIndex;
    /**
     * ArrayList containing method names extracted from the token stream by the parser logic.
     */
    private ArrayList<String> methodNames = new ArrayList<>();
    /**
     * ArrayList containing the complexity score for each method.
     */
    private ArrayList<Integer> methodComplexity=new ArrayList<>();
    /**
     * ArrayList keeping the index in the token input where the method identification exists.
     */
    private ArrayList<Integer> methodLocation =new ArrayList<>();
    private ArrayList<ParsedMethodInfo> parsedMethodInfoList=new ArrayList<>();
    public Parser(ArrayList<Token> input)
    {
        this.tokenStream=input;
    }
    @Override
    public ArrayList<ParsedMethodInfo> process() throws IOException
    {
        outputGenerate();
        return parsedMethodInfoList;
    }

    /**
     * Implements the main Recursive Descent Parser logic for defining a method.
     */
    private boolean parseMethodDefinition()
    {
        if (matchAccessModifier())
        {
            if (matchNonAccessModifier())//we have the structure access Identifier,  nonAccess modifier ---
            {
                if(matchType())
                {
                    if(matchIdentifier())
                    {
                        if(match(TokenType.PUNCTUATION,String.valueOf('(')))
                        {
                            int identifierIndex=currentTokenIndex-2;
                            Token token = tokenStream.get(identifierIndex);
                            methodLocation.add(identifierIndex);
                            methodNames.add(token.getValue());
                            return true;
                        }
                    }
                }
            }
            if(matchType())// we have the structure access identifier return type ---
            {
                if(matchIdentifier())
                {
                    if(match(TokenType.PUNCTUATION,String.valueOf('(')))
                    {
                        int identifierIndex=currentTokenIndex-2;
                        Token token = tokenStream.get(identifierIndex);
                        methodLocation.add(identifierIndex);
                        methodNames.add(token.getValue());
                        return true;
                    }
                }
            }
        }
        else if(matchNonAccessModifier()) // we have the structure nonAccess modifier return type --
        {
            if(matchType())
            {
                if(matchIdentifier())
                {
                    if(match(TokenType.PUNCTUATION,String.valueOf('(')))
                    {
                        int identifierIndex=currentTokenIndex-2;
                        Token token = tokenStream.get(identifierIndex);
                        methodLocation.add(identifierIndex);
                        methodNames.add(token.getValue());
                        return true;
                    }
                }
            }
        }

        else if(matchType()) //we have the structure identifier return type...
        {
            if(matchIdentifier())
            {
                if(match(TokenType.PUNCTUATION,String.valueOf('(')))
                {
                    int identifierIndex=currentTokenIndex-2;
                    Token token = tokenStream.get(identifierIndex);
                    methodLocation.add(identifierIndex);
                    methodNames.add(token.getValue());
                    return true;
                }
            }
        }
        return false;
    }
    private boolean match(TokenType expectedTokenType,String expectedTokenValue)
    {
        if (currentTokenIndex < tokenStream.size())
        {
            Token token = tokenStream.get(currentTokenIndex);
            if (token.getType().equals(expectedTokenType) && token.getValue().equals(expectedTokenValue)) {
                currentTokenIndex++;
                return true;
            }
        }
        return false;
    }
    private boolean matchAccessModifier()
    {
        // Check if any valid access modifier is present
        return (match(TokenType.KEYWORD,"public")
                || match(TokenType.KEYWORD,"private")
                || match(TokenType.KEYWORD, "protected"));
    }
    private boolean matchNonAccessModifier()
    {
        String[] nonAccessModifiers = {"static", "final", "abstract", "synchronised", "native", "default", "strictfp"};
        for (String modifier : nonAccessModifiers)
        {
            if (match(TokenType.KEYWORD, modifier))
            {
                return true;
            }
        }

        return false;
    }
    private boolean matchType()
    {
        String[] expectedTypes = {"int", "Integer", "boolean", "Boolean", "String", "float", "Float", "double", "Double",
                "byte", "Byte", "void", "short", "Short", "long", "Long", "char", "Character", "byte",
                "Byte", "void", "Object"};

        for (String expectedType : expectedTypes) {
            if (match(TokenType.KEYWORD, expectedType)) {
                return true;
            }
        }
        return false;
    }
    private boolean matchIdentifier()
    {
        if (currentTokenIndex < tokenStream.size())
        {
            Token token = tokenStream.get(currentTokenIndex);
            if (token.getType()==TokenType.IDENTIFIER)
            {
                    //methods.add(token.getValue());
                  //  methodStack.push(token.getValue());
                    currentTokenIndex++;
                    return true;
            }
        }
        return false;
    }
    /**
     * Iterates through the entire token input and checks for method definitions.
     */
    private void methodIdentify()
    {
        currentTokenIndex = 0;
        //consider the last one !!

        while (currentTokenIndex < tokenStream.size())
        {
            parseMethodDefinition();
            currentTokenIndex++;

        }
    }
    private boolean isCamelCase(String identifier)
    {
        // Split the identifier into words based on uppercase letters
        Pattern pattern = Pattern.compile("^[a-z]+(?:[A-Z][a-z]*)*$");
        Matcher matcher = pattern.matcher(identifier);
        if( matcher.matches())
            return true;
        else
            return false;

    }
    /**
     * Counts the number of keywords in an input token list for later computing complexity score.
     */
    private int count(ArrayList<Token> tokenList)
    {
        int totalCount = 0;
        for(int i=0; i<tokenList.size(); i++)
        {
            if(tokenList.get(i).getType()==TokenType.KEYWORD && tokenList.get(i).getValue().equals("if"))
                totalCount++;
            if(tokenList.get(i).getType()==TokenType.KEYWORD && tokenList.get(i).getValue().equals("for"))
                totalCount++;
            if(tokenList.get(i).getType()==TokenType.KEYWORD && tokenList.get(i).getValue().equals("switch"))
                totalCount++;
            if(tokenList.get(i).getType()==TokenType.KEYWORD && tokenList.get(i).getValue().equals("while"))
                totalCount++;
            if(tokenList.get(i).getType()==TokenType.KEYWORD && tokenList.get(i).getValue().equals("else"))
                totalCount++;
        }

        return totalCount;
    }
    private void countComplexityScore()
    {
        int first=0;
        int next=0;
        for(int i=0;i<methodLocation.size()-1;i++)
        {
            first=methodLocation.get(i);//the first token location number of the method,
            next=methodLocation.get(i+1);//the first token location number of next method
            ArrayList<Token> tokensBetween=new ArrayList<>();
            for(int  j=first; j<next;j++)
                tokensBetween.add(tokenStream.get(j));
            int complexityScore=count(tokensBetween);
            methodComplexity.add(complexityScore);
        }
        ArrayList<Token> tokensBetween=new ArrayList<>();
        for(int i=next; i<tokenStream.size()-1;i++)
            tokensBetween.add(tokenStream.get(i));
        int complexityScore=count(tokensBetween);
        methodComplexity.add(complexityScore);


    }
    private void outputGenerate()
    {
        methodIdentify();//puts the method identifiers in methodNames array
        countComplexityScore();//put method complexity scores in methodComplexity array list
        for (int i=0;i<methodNames.size();i++)
        {
            ParsedMethodInfo parsedMethodInfo=new ParsedMethodInfo();
            parsedMethodInfo.setMethodName(methodNames.get(i));
            parsedMethodInfo.setComplexityScore(methodComplexity.get(i));
            parsedMethodInfo.setCamelCase(isCamelCase(methodNames.get(i)));
            parsedMethodInfoList.add(parsedMethodInfo);

        }

    }
}

