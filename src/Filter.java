import java.io.IOException;
public interface Filter<T> {
    T process() throws IOException;
}

