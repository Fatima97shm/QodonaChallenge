import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;
public class Main
{
    public static void main(String[] args) throws IOException
    {
        System.out.println("Welcome To My (Very)Small Code Analyser Application!");
        System.out.println("Please Provide Me With The Absolute Path of The Source Code :)  ");
        Scanner scanner = new Scanner(System.in);
        System.out.print("File Path: ");
        String filePath = scanner.nextLine();

        Lexer lexer = new Lexer(filePath);
        Parser parser = new Parser(lexer.process());
        Evaluator evaluator = new Evaluator(parser.process());
        evaluator.process();
    }

}
