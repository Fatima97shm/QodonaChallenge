public enum TokenType {
    KEYWORD,
    LITERAL,
    PUNCTUATION,
    IDENTIFIER
    // Other token types as needed
}