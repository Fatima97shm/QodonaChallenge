import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
public class Evaluator implements Filter
{
    /**
     * The input from the parser.
     */
    private ArrayList<ParsedMethodInfo> parsedMethodInfoList;

    public Evaluator(ArrayList<ParsedMethodInfo> parsedMethodInfoList)
    {
        this.parsedMethodInfoList=parsedMethodInfoList;
    }
    @Override
    public Object process() throws IOException
    {
        printMaxThreeComplexMethods();
        printCamelCasePercentage();
        return null;
    }
    private void printMaxThreeComplexMethods ()
    {
        Collections.sort(parsedMethodInfoList, Comparator.comparingInt(ParsedMethodInfo::getComplexityScore).reversed());
        int maxMethodNameWidth = 0;
        for (ParsedMethodInfo methodInfo : parsedMethodInfoList)
        {
            int methodNameLength = methodInfo.getMethodName().length();
            maxMethodNameWidth = Math.max(maxMethodNameWidth, methodNameLength);
        }

        System.out.println("Top Three Method With The Maximum Complexity Scores Are As Followed: ");
        System.out.println();
        System.out.printf("%-" + (maxMethodNameWidth + 15) + "sComplexity Score\n", "Method Name");
        int count=0;
        for (ParsedMethodInfo methodInfo : parsedMethodInfoList) {
            if (count >= 3)
            {
                break;
            }
            String methodName = methodInfo.getMethodName();
            int complexityScore = methodInfo.getComplexityScore();
            System.out.printf("%-" + (maxMethodNameWidth + 15) + "s%d\n", methodName , complexityScore);
            count++;
        }
    }
    private void printCamelCasePercentage()
    {
        int totalMethodCount = parsedMethodInfoList.size();
        int camelCaseCount = 0;
        for (ParsedMethodInfo methodInfo : parsedMethodInfoList)
        {
            if (methodInfo.isCamelCase())
            {
                camelCaseCount++;
            }
        }
        int nonAdheringCount = totalMethodCount - camelCaseCount;
        double nonAdheringPercentage = (double) nonAdheringCount / totalMethodCount * 100;
        System.out.println();
        System.out.println("Percentage Of Methods Not Adhering To CamelCase: " +nonAdheringPercentage + " %");
    }


}