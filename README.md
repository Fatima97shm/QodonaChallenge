**Code Analyser Java Application**
#### 1. How to Run the Application
- **Using the Jar File**:  Execute the jar file, and further instructions will be provided within the application. You only need to provide the path to the source code.
- **Using the Project Class**:Run the main class of the project. You will be prompted to provide the absolute path to the source code.


#### 2. Structure of the Application
- **Application Architecture**: The application follows a Pipe and Filter architecture.
- **Main Classes**:
    - Lexer
    - Parser
    - Evaluator
- **Supporting Classes**:
    - Token: Represents tokens generated by the Lexer class.
    - ParsedMethodInfo: Represents the output data structure of the Lexer and Parser classes
- **Filter Interface**:
  - A Filter interface is implemented to follow  the Pipe and Filter architecture. 
- **Code Structure**:
  - I aimed for modularity. There exists high cohesion in a single class and low coupling between different classes.
- **Testing**:
  - I addressed some edge cases through tests. But due to time constraints, I couldn't address all of them. Unfortunately, most of my tests are not located in the test directory due to limitations with JUnit in testing private methods.




  #### 3. Design Decisions & Limitations 

- **Development Tools**: I developed the lexer and parser without a symbol table for simplicity and not using tools like JFlex to better asses my coding skilks. 
- **Lexer Class**:
    - Firstly comments are ommited
    - Input is tokenize the into four simple types of tokens: keywords, punctuation, literals, and identifiers. 
    - The lexer outputs an ArrayList of tokens.
    - A buffer system is implemented using two buffers, each approximately the size of a disk block, to load the input.
- **Parser Class**:
  - The method definition parsing logic is written using recursive descent method.
  - The grammar used for parsing is as followed: 
       - ![img_1.png](img_1.png)
  - One significant limitation of my application is in handling the return type of method. Currently, the application only accepts return types defined by keywords.
  - Given the open-ended nature of the project, I made numerous trade-offs throughout the codebase.
  - To avoid parsing the entire code, I decided that everything between two method definitions is a method body. I acknowledge that in reality, there could be nested classes or methods returning class types. But due to timing constraints, I made this design decision.
  - For camel case detection, I used Java's regex. However solely relying on regex makes it impossible to determine if an identifier entirely in lowercase is in camel case. To achieve this, language models would be necessary.
- **Evaluator Class**:
    - The evaluator class is straightforward. It takes as input an ArrayList of ParsedMethodInfo objects, which I created to store method name, complexity score, and whether it is camel case. It then processes this information and displays the output to the user.




